-- Your SQL goes here
CREATE TABLE users (
    user_id SERIAL NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    date_registered timestamptz NOT NULL,

    PRIMARY KEY(user_id),
    UNIQUE(username)
);