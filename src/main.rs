#[macro_use]
extern crate diesel;

#[macro_use]
extern crate async_trait;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

mod consts; use consts::*;
mod server; use server::*;
mod message; use message::*;
mod connection; use connection::*;
mod user; use user::*;
mod db; use db::*;
mod types; use types::*;
mod parse;
mod requests;
mod bucket;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    fern::Dispatch::new()
        .format(|out, message, record| {
            let level_colour = match record.level() {
                log::Level::Error => "\x1b[38;5;9m",
                log::Level::Warn => "\x1b[38;5;11m",
                log::Level::Debug => "\x1b[38;5;14m",
                
                log::Level::Info | 
                log::Level::Trace => "",
            };

            out.finish(format_args!(
                "\x1b[38;5;238m{}\x1b[0m [{}] {}{}\x1b[0m: {}",
                chrono::Utc::now().format("<%F %T>"),
                record.module_path().unwrap_or(record.target()),
                level_colour, record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(fern::log_file(format!("logs/log_{}.txt", chrono::Utc::now().timestamp())).expect("Failed to open log file."))
        .apply()
        .expect("Failed to create fern dispatch.");

    let db = Db::new();

    let server = Server::connect(db).await;

    info!("Listening...");

    while let Err(e) = server.accept_clients().await {
        error!("Server Error: {:?}", e);
    }
}