use chrono::{ DateTime, Utc };

use crate::db::schema::users;
use diesel::prelude::*;

use crate::db::*;

#[derive(Clone, Debug, Queryable, AsChangeset, Identifiable)]
#[primary_key(user_id)]
#[table_name = "users"]
pub struct DbUser {
    pub user_id: i32,
    pub username: String,
    pub password: String,
    pub date_registered: DateTime<Utc>,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct DbUserInsert {
    pub username: String,
    pub password: String,
    pub date_registered: DateTime<Utc>,
}

impl DbUser {
    pub fn new(user_id: i32, username: String, password: String, date_registered: DateTime<Utc>) -> Self {
        DbUser {
            user_id,
            username,
            password,
            date_registered,
        }
    }
}

impl Db {
    pub async fn _get_user(&self, user_id: i32) -> QueryResult<Option<DbUser>> {
        users::table.find(user_id)
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn find_user(&self, username: String) -> QueryResult<Option<DbUser>> {
        users::table.filter(users::username.eq(username))
            .first(&*self.conn.lock().await).optional()
    }

    pub async fn add_user(&self, user: DbUserInsert) -> QueryResult<()> {
        diesel::insert_into(users::table).values(&user).execute(&*self.conn.lock().await)?;

        Ok(())
    }
}