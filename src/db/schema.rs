table! {
    users (user_id) {
        user_id -> Int4,
        username -> Text,
        password -> Text,
        date_registered -> Timestamptz,
    }
}
