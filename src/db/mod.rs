use diesel::prelude::*;

use tokio::sync::Mutex;

pub mod schema;
mod users; pub use users::*;

pub struct Db {
    pub conn: Mutex<PgConnection>,
}

impl Db {
    pub fn new() -> Db {
        let config = std::env::var("DATABASE_URL").expect("Config required in environmental variable DATABASE_URL.");
        let conn = PgConnection::establish(&config).expect("Could not connect to database.");

        Db { conn: Mutex::new(conn) }
    }
}