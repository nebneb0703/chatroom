use chrono::{ DateTime, Utc, TimeZone };

use serde::{ Serialize, Serializer, Deserialize, Deserializer, de::Visitor };

pub struct UtcDateTime(pub DateTime<Utc>);

impl Serialize for UtcDateTime {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where S: Serializer {
        serializer.serialize_i64(self.0.timestamp_millis())
    }
}

impl<'de> Deserialize<'de> for UtcDateTime {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> 
    where
        D: Deserializer<'de> 
    {
        deserializer.deserialize_i64(UtcDateTimeVisitor)
    }
}

struct UtcDateTimeVisitor;

impl<'de> Visitor<'de> for UtcDateTimeVisitor {
    type Value = UtcDateTime;

    fn expecting(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(formatter, "Chrono timestamp in milliseconds: number of non-leap-milliseconds since January 1, 1970 UTC.")
    }

    fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E> 
    where
        E: serde::de::Error
    {
        Ok(UtcDateTime(Utc.timestamp_millis(v)))
    }

    fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E> 
    where
        E: serde::de::Error
    {
        Ok(UtcDateTime(Utc.timestamp_millis(v as i64)))
    }
}

impl From<DateTime<Utc>> for UtcDateTime {
    fn from(t: DateTime<Utc>) -> UtcDateTime {
        UtcDateTime(t)
    }
}