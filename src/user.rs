use std::net::SocketAddr;

use crate::DbUser;

#[derive(Clone)]
pub struct User {
    pub address: SocketAddr,
    pub account: DbUser,
}