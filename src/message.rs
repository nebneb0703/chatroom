use crate::Connection;

use tungstenite::Message;

#[derive(Clone)]
pub struct ClientMessage {
    pub connection: Connection,
    pub inner: Message,
}