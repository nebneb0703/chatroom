use std::net::{ 
    SocketAddr, IpAddr
};

use std::sync::Arc;

use tokio::sync::Mutex;

use futures_util::{
    SinkExt,
    stream::{ SplitSink, SplitStream }
};

use tungstenite::Message;
use bson::doc;

use crate::{ 
    WebSocket, ServerProperties, get_logged_in, 
    requests::LoginBroadcast,
    bucket::Bucket,
};

use bson::to_document;

use chrono::Utc;

pub type ReadSocket = SplitStream<WebSocket>;
pub type WriteSocket = SplitSink<WebSocket, Message>;

#[derive(Clone)]
pub struct Connection {
    pub read: Arc<Mutex<ReadSocket>>,
    pub write: Arc<Mutex<WriteSocket>>,
    pub address: SocketAddr,

    pub connection_info: ConnectionInfo,

    pub bucket: Arc<Mutex<Bucket>>,

    disconnected: Arc<Mutex<bool>>
}

impl Connection {
    pub fn new(read: Arc<Mutex<ReadSocket>>, write: Arc<Mutex<WriteSocket>>, address: SocketAddr, connection_info: ConnectionInfo) -> Self {
        Connection {
            read,
            write,
            address,
            disconnected: Arc::new(Mutex::new(false)),
            connection_info,
            bucket: Arc::new(Mutex::new(Bucket::new(crate::BUCKET_DURATION, crate::BUCKET_LIMIT))),
        }
    }

    pub async fn on_disconnect(self, properties: ServerProperties) {
        {
            let mut clients = properties.connected_clients.lock().await;

            if clients.contains_key(&self.address) {
                clients.remove(&self.address);
            }
        }

        let mut users = properties.connected_users.lock().await;

        if let Some((session_uuid, old_user)) = users.clone().iter().filter(|(_k, v)| v.address == self.address).next() {
            debug!("User {}@{} logged out.", old_user.account.username, self);

            let user = old_user.clone();
            // Log out
            users.remove(session_uuid);

            drop(users);

            let broadcast = LoginBroadcast {
                username: user.account.username.clone(),
                time: Utc::now().into(),
                logged_in: true,
            };
    
            let response = doc! {
                "type": "broadcast",
                "broadcast": "login",
                "body": to_document(&broadcast).expect("Failed to convert broadcast to document."),
            };
    
            let mut response_bytes = Vec::new();
    
            response.to_writer(&mut response_bytes).expect("Failed to write message response to bytes.");
    
            for client in get_logged_in(properties.clone()).await {
                match client.write.lock().await.send(Message::Binary(response_bytes.clone())).await{
                    Ok(_) => {},
                    Err(e) => {
                        error!("Failed to broadcast disconnect to socket {} - {:?}", client, e);
                    }
                }
            }
        }

        if let Err(e) = self.write.lock().await.close().await {
            warn!("Error closing connection {}: {:?}", self, e);
        }

        *self.disconnected.lock().await = true;
    }

    pub async fn disconnected(&self) -> bool {
        *self.disconnected.lock().await
    }

    pub async fn reply_message<S1,S2>(&self, response_type: S1, message: S2)
    where
        S1: Into<String>,
        S2: Into<String>,
    {
        if *self.disconnected.lock().await { return; }

        let s1 = response_type.into();
        let s2 = message.into();

        let doc = doc! {
            "type": s1.clone(),
            "body": {
                "message": s2.clone()
            }
        };

        let mut bytes = Vec::new();

        doc.to_writer(&mut bytes).expect("Failed to write message to bytes.");

        match self.write.lock().await.send(Message::Binary(bytes)).await {
            Ok(_) => {},
            Err(e) => {
                error!("Failed to send {} '{}' to {} - {:?}", s1, s2, self, e);
            }
        }
    }
}

#[derive(Clone)]
pub struct ConnectionInfo {
    pub real_address: Option<IpAddr>,
    pub country: Option<String>,
}

impl Default for ConnectionInfo {
    fn default() -> ConnectionInfo {
        ConnectionInfo {
            real_address: None,
            country: None,
        }
    }
}

use std::fmt;

impl fmt::Display for Connection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}{}",
            match self.connection_info.real_address {
                Some(a) => a.to_string(),
                None => self.address.to_string(),
            },
            if let Some(country) = &self.connection_info.country {
                format!(" from {}", country)
            } else { "".to_owned() },
            if let Some(_) = self.connection_info.real_address {
                format!(" ({})", self.address)
            } else { "" .to_owned() },
        )
    }
}
