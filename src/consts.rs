pub const WELCOME_MESSAGE: &str = 
"Welcome to Nebula's Secret Chatroom!

Please log in or register.";

pub const MAX_USERNAME_LENGTH: usize = 16; // Characters
pub const MAX_PASSWORD_LENGTH: usize = 16; // Characters

pub const MIN_USERNAME_LENGTH: usize = 4; // Characters
pub const MIN_PASSWORD_LENGTH: usize = 6; // Characters

pub const SALT_LENGTH: usize = 16; // Bytes

pub const MAX_MESSAGE_LENGTH: usize = 2024; // Characters

pub const BUCKET_DURATION: i64 = 5; // Seconds
pub const BUCKET_LIMIT: usize = 10; // Requests
