use std::collections::VecDeque;

use chrono::{ DateTime, Utc, Duration };

#[derive(Clone)]
pub struct Bucket {
    duration: i64,
    limit: usize,

    previous: VecDeque<DateTime<Utc>>,
}

impl Bucket {
    pub fn new(duration: i64, limit: usize) -> Self {
        Bucket {
            duration,
            limit,

            previous: VecDeque::new(),
        }
    }

    pub fn pass_now(&mut self) -> bool {
        self.clear_old();

        self.previous.push_back(Utc::now());

        self.previous.len() < self.limit
    }

    fn clear_old(&mut self) {
        let threshold = Utc::now() - Duration::seconds(self.duration);

        while let Some(&first) = self.previous.front() {
            if first > threshold {
                break;
            }
            
            self.previous.pop_front().unwrap();
        } 
    }
}