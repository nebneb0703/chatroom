use futures_util::{
    stream::StreamExt,
    sink::SinkExt,
};

use tokio_tungstenite::accept_hdr_async;
use tungstenite::{ Message, Error };

use tokio::net::TcpListener;

use std::sync::Arc;
use tokio::sync::Mutex;

use tokio::sync::mpsc;

use std::collections::HashMap;
use std::net::SocketAddr;

use uuid::Uuid;

use bson::doc;

use crate::{ 
    Db, Connection, ConnectionInfo, User, ClientMessage,
};

pub type ConnectedClients = Arc<Mutex<HashMap<SocketAddr, Connection>>>;
pub type ConnectedUsers = Arc<Mutex<HashMap<Uuid, User>>>;
pub type WebSocket = tokio_tungstenite::WebSocketStream<tokio::net::TcpStream>;

#[derive(Clone)]
pub struct ServerProperties {
    pub database: Arc<Db>,

    pub connected_clients: ConnectedClients,
    pub connected_users: ConnectedUsers,
}

pub struct Server {
    listener: TcpListener,

    welcome_message: Vec<u8>,

    pub properties: ServerProperties,
}

impl Server {
    pub async fn connect(database: Db) -> Self {
        let listener = TcpListener::bind("0.0.0.0:7777").await.expect("Failed to bind TcpListener to localhost:7777");

        let connected_clients: ConnectedClients = Arc::new(Mutex::new(HashMap::new()));
        let connected_users: ConnectedUsers = Arc::new(Mutex::new(HashMap::new()));
    
        let properties = ServerProperties { database: Arc::new(database), connected_clients, connected_users };

        let mut welcome_message_bytes = Vec::new();

        let welcome_message_doc = doc! {
            "type": "motd",
            "body": {
                "motd": crate::WELCOME_MESSAGE,
            }
        };

        welcome_message_doc.to_writer(&mut welcome_message_bytes).expect("Failed to write welcome message to bytes.");

        Server {
            listener, properties,
            welcome_message: welcome_message_bytes,
        }
    }

    pub async fn accept_clients(&self) -> Result<(), tungstenite::Error> {
        let (tx, rx) = mpsc::channel::<ClientMessage>(32);

        tokio::spawn(crate::parse::parse_messages(self.properties.clone(), rx));

        loop {
            let tx_cloned = tx.clone();

            let (stream, address) = self.listener.accept().await?;
    
            info!("New connection received from {}", address); 

            let mut connection_info = ConnectionInfo::default();

            let mut socket = accept_hdr_async(stream, |req: &Request, res| {
                process_headers(&mut connection_info, req, res)
            }).await?;                    
    
            debug!("Successfully performed handshake for {}", address);
    
            socket.send(Message::Binary(self.welcome_message.clone())).await?;
    
            debug!("Sent welcome message {}", address);

            let (write, read) = socket.split();

            let connection = Connection::new(
                Arc::new(Mutex::new(read)),
                Arc::new(Mutex::new(write)),
                address,
                connection_info
            );

            let connection_clone = connection.clone();

            {
                let mut lock = self.properties.connected_clients.lock().await;

                lock.insert(address, connection);
            }

            debug!("Added to connected clients {}", address);
    
            let properties = self.properties.clone();

            tokio::spawn(async move {
                loop {
                    if connection_clone.disconnected().await {
                        warn!("Socket {} already disconnected, ending read.", connection_clone);
                        return;
                    }

                    let option = { 
                        let mut lock = connection_clone.read.lock().await;
                        
                        lock.next().await
                    };
            
                    let result = match option {
                        Some(r) => r,
                        None => {
                            warn!("Received None from socket {}.", connection_clone);
                            connection_clone.on_disconnect(properties.clone()).await;

                            return;
                        }
                    };
            
                    let message = match result {
                        Ok(m) => m.clone(),
                        Err(Error::ConnectionClosed) => {
                            connection_clone.on_disconnect(properties.clone()).await;

                            return;
                        }
                        Err(Error::AlreadyClosed) => {
                            connection_clone.on_disconnect(properties.clone()).await;

                            return;
                        }
                        Err(Error::Io(e)) => {
                            warn!("Fatal IO Error from socket {}: {:?}", connection_clone, e);

                            connection_clone.on_disconnect(properties.clone()).await;

                            return;
                        }
                        Err(e) => {
                            warn!("Error from socket {}: {:?}", connection_clone, e);

                            continue;
                        }
                    };

                    let client_message = ClientMessage {
                        connection: connection_clone.clone(),
                        inner: message,
                    };

                    if let Err(_) = tx_cloned.send(client_message).await {
                        warn!("Channel Receiver dropped for socket {}.", connection_clone);

                        connection_clone.on_disconnect(properties.clone()).await;

                        return;
                    }
                }
            });
        }
    }
}

pub async fn get_logged_in(properties: ServerProperties) -> Vec<Connection> {
    let logged_in: Vec<SocketAddr> = properties.connected_users.lock().await
        .values().map(|x| x.address).collect();

    properties.connected_clients.lock().await.values()
        .filter(|x| logged_in.contains(&x.address))
        .map(|x| x.clone()).collect()
}

use tungstenite::handshake::server::{
    Request, Response, ErrorResponse,
};

pub fn process_headers<'r>(connection_info: &mut ConnectionInfo, request: &'r Request, response: Response) -> Result<Response, ErrorResponse> {
    let real_address = match request.headers().get("cf-connecting-ip") {
        None => None,
        Some(value) => {
            match value.to_str() {
                Ok(s) => match s.parse() {
                    Ok(addr) => {
                        info!("Real Address: {}", addr);
                        Some(addr)
                    },
                    Err(_) => None,
                },
                Err(_) => None,
            }
        }
    };

    let country = match request.headers().get("cf-ipcountry") {
        None => None,
        Some(value) => {
            match value.to_str() {
                Ok(s) => {
                    info!("Country: {}", s);
                    Some(s.to_owned())
                },
                Err(_) => None,
            }
        }
    };

    connection_info.real_address = real_address;
    connection_info.country = country;

    Ok(response)
}
