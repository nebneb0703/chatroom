use std::collections::HashMap;

use crate::requests::*;

macro_rules! insert_map {
    ($map:ident, $($handler:expr),+ $(,)?) => {
        $(
            $map.insert(
                $handler.request_type(),
                Box::new($handler)
            );
        )+
    };
}

lazy_static! {
    pub static ref REQUEST_TYPES: HashMap<&'static str, Box<dyn RequestHandler>> = {
        let mut map: HashMap<&'static str, Box<dyn RequestHandler>> = HashMap::new();

        insert_map!{ map, 
            PingRequestHandler,
            LoginRequestHandler,
            RegisterRequestHandler,
            MessageRequestHandler,
        };

        map
    };
}