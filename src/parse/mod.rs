use futures_util::{ SinkExt };

use tungstenite::Message;

use bson::Document;

use crate::{ ServerProperties, ClientMessage };

use tokio::sync::mpsc::{ Receiver };

mod types; pub use types::*;

pub async fn parse_messages(properties: ServerProperties, mut rx: Receiver<ClientMessage>) {
    loop {
        let message = match rx.recv().await {
            Some(m) => m,
            None => {
                warn!("Received None on channel.");
                return;
            }
        };

        match message.inner {
            Message::Binary(bytes) => {
                let document = match Document::from_reader(&mut bytes.as_slice()) {
                    Ok(d) => d,
                    Err(e) => {
                        message.connection.reply_message("error", format!("Invalid request: {:?}", e)).await;
                        continue;
                    }
                };

                if !message.connection.bucket.lock().await.pass_now() {
                    warn!("Rate limitting connection {}", message.connection);
                    message.connection.reply_message("warn", "Woah calm down! Sending requests too fast!").await;
                    continue;
                }
                
                let request_type = match document.get_str("type") {
                    Ok(req_type) => req_type,
                    Err(_e) => {
                        message.connection.reply_message("error", "Invalid BSON format: missing or invalid 'type' field.").await;
                        continue;
                    }
                };

                let body = match document.get_document("body") {
                    Ok(req_type) => req_type,
                    Err(_e) => {
                        message.connection.reply_message("error", "Invalid BSON format: missing or invalid 'body' field.").await;
                        continue;
                    }
                };
                
                let handler = match REQUEST_TYPES.get(request_type) {
                    Some(h) => h,
                    None => {
                        message.connection.reply_message("error", format!("Invalid BSON format: type '{}' is not a valid request type.", request_type)).await;
                        continue;
                    }
                };
                
                handler.process(properties.clone(), &message.connection, &body).await;
            }

            Message::Ping(_) => {
                match message.connection.write.lock().await
                    .send(Message::Pong(vec![])).await 
                {
                    Ok(_) => {},
                    Err(e) => {
                        error!("Failed to send Pong to {} - {:?}", message.connection, e);
                    }
                }
            }

            Message::Pong(_) => {
                match message.connection.write.lock().await
                    .send(Message::Ping(vec![])).await 
                {
                    Ok(_) => {},
                    Err(e) => {
                        error!("Failed to send Ping to {} - {:?}", message.connection, e);
                    }
                }
            }

            Message::Close(_) => {
                info!("Connection closed {}", message.connection);

                message.connection.on_disconnect(properties.clone()).await;
            }

            _ => {
                info!("Received from {}: {:?}", message.connection, message.inner);
            }
        }
    }
}
