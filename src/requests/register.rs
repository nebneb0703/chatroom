use serde::{ Serialize, Deserialize };
use bson::{ doc, Document, from_document };

use argon2::{ self, Config };
use rand_chacha::ChaChaRng;
use rand::prelude::*;

use chrono::Utc;

use crate::{ 
    DbUserInsert, ServerProperties, requests::RequestHandler, Connection,
    SALT_LENGTH,
};

#[derive(Serialize, Deserialize)]
pub struct RegisterRequest {
    pub username: String,
    pub password: String,
}

#[derive(Clone)]
pub struct RegisterRequestHandler;

#[async_trait]
impl RequestHandler for RegisterRequestHandler {
    fn request_type(&self) -> &'static str {
        "register"
    }

    async fn process(&self, properties: ServerProperties, connection: &Connection, document: &Document){
        let request: RegisterRequest = match from_document(document.clone()) {
            Ok(r) => r,
            Err(e) => {
                connection.reply_message("error", format!("Invalid Register Request Body - {:?}", e)).await;
                return;
            }
        };

        if request.username.contains(" ") {
            connection.reply_message("error", "Username cannot contain spaces.").await;
            return;
        }

        if request.username.len() > crate::MAX_USERNAME_LENGTH || request.username.len() < crate::MIN_USERNAME_LENGTH {
            connection.reply_message("error", format!("Username length out of range [{}-{}]", crate::MIN_USERNAME_LENGTH, crate::MAX_USERNAME_LENGTH)).await;
            return;
        }

        if request.password.len() > crate::MAX_PASSWORD_LENGTH || request.password.len() < crate::MIN_PASSWORD_LENGTH {
            connection.reply_message("error", format!("Password length out of range [{}-{}]", crate::MIN_PASSWORD_LENGTH, crate::MAX_PASSWORD_LENGTH)).await;
            return;
        }

        if let Some(_) = properties.database.find_user(request.username.clone()).await.expect("Database error") {
            connection.reply_message("error", format!("Username '{}' already exists.", request.username)).await;
            return;
        }

        let mut salt = [0; SALT_LENGTH];

        let mut rng = ChaChaRng::from_entropy();

        rng.fill_bytes(&mut salt);

        let argon2_config = Config::default();

        let password_hash = argon2::hash_encoded(request.password.as_bytes(), &salt, &argon2_config).expect("Failed to hash password with Argon2i.");

        let user = DbUserInsert {
            username: request.username.clone(),
            password: password_hash,
            date_registered: Utc::now()
        };

        properties.database.add_user(user).await.expect("Failed to add user to db.");

        connection.reply_message("info", format!("Successfully registered user '{}'.", request.username)).await;
    }
}