use futures_util::SinkExt;

use tungstenite::Message;

use serde::{ Serialize, Deserialize };
use bson::{ doc, Document, to_document, from_document, Bson, Binary, spec::BinarySubtype };

use chrono::Utc;

use uuid::Uuid;

use crate::{ ServerProperties, requests::RequestHandler, Connection, get_logged_in, User, UtcDateTime };

#[derive(Serialize, Deserialize)]
pub struct LoginRequest {
    pub username: String,
    pub password: String,
}

#[derive(Serialize)]
pub struct LoginBroadcast {
    pub username: String,
    pub time: UtcDateTime,
    pub logged_in: bool,
}

#[derive(Clone)]
pub struct LoginRequestHandler;

#[async_trait]
impl RequestHandler for LoginRequestHandler {
    fn request_type(&self) -> &'static str {
        "login"
    }

    async fn process(&self, properties: ServerProperties, connection: &Connection, document: &Document){
        let request: LoginRequest = match from_document(document.clone()) {
            Ok(r) => r,
            Err(e) => {
                connection.reply_message("error",  format!("Invalid Login Request Body - {:?}", e)).await;
                return;
            }
        };

        let account = match properties.database.find_user(request.username.clone()).await.expect("Failed to get DbUser.") {
            Some(u) => u,
            None => {
                connection.reply_message("error", format!("User '{}' does not exist.", request.username)).await;
                return;
            }
        };

        if !argon2::verify_encoded(&account.password, request.password.as_bytes()).expect("Failed to verify password with Argon2i.") { 
            connection.reply_message("error", "User and password do not match.").await;
            return;
        }

        let uuid = Uuid::new_v4();

        let user = User {
            address: connection.address,
            account: account.clone()
        };

        {
            let mut connected_users = properties.connected_users.lock().await;
            connected_users.insert(uuid, user);
        }
        
        let bson_uuid = Bson::Binary(Binary {
            subtype: BinarySubtype::Uuid,
            bytes: uuid.as_bytes().to_vec()
        });

        let logged_in: Vec<String> = properties.connected_users.lock().await.values()
            .map(|x| x.account.username.clone()).collect();

        let response = doc!{
            "type": "login",
            "body": {
                "session_uuid": bson_uuid,
                "logged_in_users": logged_in,
            }
        };

        let mut response_bytes = Vec::new();

        response.to_writer(&mut response_bytes).expect("Failed to write login response to bytes.");

        match connection.write.lock().await.send(Message::Binary(response_bytes)).await {
            Ok(_) => {},
            Err(e) => {
                error!("Failed to send login response to {} - {:?}", connection, e);
                return;
            }
        }

        let broadcast = LoginBroadcast {
            username: account.username.clone(),
            time: Utc::now().into(),
            logged_in: true,
        };

        let response = doc! {
            "type": "broadcast",
            "broadcast": "login",
            "body": to_document(&broadcast).expect("Failed to convert broadcast to document."),
        };

        let mut response_bytes = Vec::new();

        response.to_writer(&mut response_bytes).expect("Failed to write message response to bytes.");

        debug!("User {}@{} logged in.", account.username, connection);

        for client in get_logged_in(properties).await {
            match client.write.lock().await.send(Message::Binary(response_bytes.clone())).await {
                Ok(_) => {},
                Err(e) => {
                    error!("Failed to broadcast disconnect to socket {} - {:?}", client, e);
                }
            }
        }
    }
}