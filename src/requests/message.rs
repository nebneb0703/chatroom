use futures_util::SinkExt;

use tungstenite::Message;

use serde::{ Serialize, Deserialize };
use bson::{ doc, Document, from_document, to_document, Bson, spec::BinarySubtype, serde_helpers::uuid_as_binary };

use chrono::Utc;
use uuid::Uuid;

use crate::{ ServerProperties, requests::RequestHandler, Connection, get_logged_in, UtcDateTime };

#[derive(Serialize, Deserialize)]
pub struct MessageRequest {
    #[serde(with = "uuid_as_binary")]
    pub session_uuid: Uuid,
    pub message: String,
    pub time_sent: UtcDateTime,
}

#[derive(Serialize)]
pub struct MessageBroadcast {
    pub username: String,
    pub message: String,
    pub time_sent_client: UtcDateTime,
    pub time_sent_server: UtcDateTime,
}

#[derive(Clone)]
pub struct MessageRequestHandler;

#[async_trait]
impl RequestHandler for MessageRequestHandler {
    fn request_type(&self) -> &'static str {
        "message"
    }

    async fn process(&self, properties: ServerProperties, connection: &Connection, document: &Document){
        let cloned = document.clone();
        
        let request: MessageRequest = match from_document(cloned) {
            Ok(r) => r,
            Err(e) => {
                connection.reply_message("error", format!("Invalid Message Request Body - {:?}", e)).await;
                return;
            }
        };

        if request.message.len() > crate::MAX_MESSAGE_LENGTH {
            connection.reply_message("error", format!("Chat message cannot be more than {} bytes.", crate::MAX_MESSAGE_LENGTH)).await;
                return;
        }

        let user = {
            let connected_users = properties.connected_users.lock().await;
            
            match connected_users.get(&request.session_uuid) {
                Some(u) => u.clone(),
                None => {
                    connection.reply_message("error", "Invalid session uuid.").await;
                    return;
                }
            }
        };

        let broadcast = MessageBroadcast {
            username: user.account.username.clone(),
            message: request.message,
            time_sent_client: request.time_sent,
            time_sent_server: Utc::now().into(),
        };

        let response = doc! {
            "type": "broadcast",
            "broadcast": "message",
            "body": to_document(&broadcast).expect("Failed to convert broadcast to document."),    
        };

        let mut response_bytes = Vec::new();

        response.to_writer(&mut response_bytes).expect("Failed to write message response to bytes.");

        for client in get_logged_in(properties).await {
            match client.write.lock().await.send(Message::Binary(response_bytes.clone())).await {
                Ok(_) => {},
                Err(e) => {
                    error!("Failed to broadcast disconnect to socket {} - {:?}", client, e);
                }
            }
        }
    }
}