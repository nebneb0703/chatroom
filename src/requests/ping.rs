use futures_util::SinkExt;

use tungstenite::Message;

use bson::{ doc, Document };


use crate::{ ServerProperties, requests::RequestHandler, Connection };

#[derive(Clone)]
pub struct PingRequestHandler;

#[async_trait]
impl RequestHandler for PingRequestHandler {
    fn request_type(&self) -> &'static str {
        "ping"
    }

    async fn process(&self, _properties: ServerProperties, connection: &Connection, _document: &Document){
        let response = doc!{
            "type": "pong",
            "body": { }
        };

        let mut response_bytes = Vec::new();

        response.to_writer(&mut response_bytes).expect("Failed to write pong response to bytes.");

        match connection.write.lock().await.send(Message::Binary(response_bytes)).await {
            Ok(_) => {},
            Err(e) => {
                error!("Failed to send pong response to {} - {:?}", connection, e);
                return;
            }
        }
    }
}
