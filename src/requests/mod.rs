mod login; pub use login::*;
mod register; pub use register::*;
mod message; pub use message::*;
mod ping; pub use ping::*;

use bson::Document;

use crate::{ Connection, ServerProperties };

#[async_trait]
pub trait RequestHandler : Sync {
    fn request_type(&self) -> &'static str;

    async fn process(&self, properties: ServerProperties, connection: &Connection, document: &Document);
}