# Chatroom

The server is hosted on `wss://nebula.ayyeve.xyz`.

## General Information

### BSON Format

All requests and responses will have a `type` field, defining the type of request or response, and a `body` field, defining the contents of the request or response.

### Types

`BinData` is a BSON byte stream format.
`Timestamp` is number of non-leap-milliseconds since January 1, 1970 UTC.

### Errors

Any errors will have a `type` field of `error` and a `message` field in the body containing the error message, similarly to the info response.

### Broadcasts

There is a distinction in the format of the response between a broadcast and a normal response.

A broadcast can be received at any point during the time you are connected to the server.
A standard response will be received immediately after a request has been made.

The client should be ready to receive broadcasts at any time, but can assume and expect a standard response if a request was sent.

## Connecting

Immediately after the WebSocket handshake, the server will reply with a motd BSON object:

```json
{
    "type": "motd",
    "body": {
        "motd": "Welcome!"
    }
}
```

## Register Account

```json
{
    "type": "register",
    "body": {
        "username": "test",
        "password": "123445678"
    }
}
```

The username must not contain spaces, and have a length between 4 and 16 UTF-8 characters.

The password must have a length of between 6 and 16 UTF-8 characters.

You will receive a response in this form:

```json
{
    "type": "info",
    "body": {
        "message": "Successfully registered user 'test'."
    }
}
```

## Logging in

```json
{
    "type": "login",
    "body": {
        "username": "test",
        "password": "123445678"
    }
}
```

You will receive a response that looks like this:

```json
{
    "type": "login",
    "body": {
        "session_uuid": BinData,
        "logged_in_users": ["test","123"]
    }
}
```

where `BinData` is marked as UUID. The UUID consists of 16 bytes. This UUID needs to be captured and saved for the session. It is a one time key that allows the client to send messages without having to provide a password every time.

where `logged_in_users` is a list of usernames.

You will also receive a broadcast (which is sent to all logged in users):
```json
{
    "type": "broadcast",
    "broadcast": "login",
    "body": {
        "username": "test",
        "time": Timestamp,
        "logged_in": true,
    }
}
```

where `logged_in` refers to if the user just logged in or logged out.

## Sending Messages

```json
{
    "type": "message",
    "body": {
        "session_uuid": BinData,
        "message": "Hello World!",
        "time_sent": Timestamp,
    }
}
```

You will receive a broadcast (which is sent to all logged in users):

```json
{
    "type": "broadcast",
    "broadcast": "message",
    "body": {
        "username": "test",
        "message": "Hello World!",
        "time_sent_client": Timestamp,
        "time_sent_server": Timestamp
    }
}
```

where `time_sent_client` is the value provided by the message request, and `time_sent_server` is the time when the request reached the server.